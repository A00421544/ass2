﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ContactForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.bn_next = New System.Windows.Forms.Button()
        Me.tb_fname = New System.Windows.Forms.TextBox()
        Me.bn_prev = New System.Windows.Forms.Button()
        Me.tb_index = New System.Windows.Forms.TextBox()
        Me.PrimaryKey = New System.Windows.Forms.Label()
        Me.FirstName = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.bn_browse = New System.Windows.Forms.Button()
        Me.csvimport = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tb_lname = New System.Windows.Forms.TextBox()
        Me.LastName = New System.Windows.Forms.Label()
        Me.tb_snum = New System.Windows.Forms.TextBox()
        Me.tb_city = New System.Windows.Forms.TextBox()
        Me.tb_province = New System.Windows.Forms.TextBox()
        Me.StreetNumber = New System.Windows.Forms.Label()
        Me.City = New System.Windows.Forms.Label()
        Me.Province = New System.Windows.Forms.Label()
        Me.tb_country = New System.Windows.Forms.TextBox()
        Me.Country = New System.Windows.Forms.Label()
        Me.PostalCode = New System.Windows.Forms.Label()
        Me.PhoneNumber = New System.Windows.Forms.Label()
        Me.EmailAddress = New System.Windows.Forms.Label()
        Me.tb_postalcode = New System.Windows.Forms.TextBox()
        Me.tb_phonenumber = New System.Windows.Forms.TextBox()
        Me.tb_email = New System.Windows.Forms.TextBox()
        Me.bn_upload = New System.Windows.Forms.Button()
        Me.tb_status = New System.Windows.Forms.Label()
        Me.Status = New System.Windows.Forms.TextBox()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'bn_next
        '
        Me.bn_next.Location = New System.Drawing.Point(275, 280)
        Me.bn_next.Name = "bn_next"
        Me.bn_next.Size = New System.Drawing.Size(75, 32)
        Me.bn_next.TabIndex = 0
        Me.bn_next.Text = "Next"
        Me.bn_next.UseVisualStyleBackColor = True
        '
        'tb_fname
        '
        Me.tb_fname.Location = New System.Drawing.Point(80, 38)
        Me.tb_fname.Name = "tb_fname"
        Me.tb_fname.Size = New System.Drawing.Size(276, 20)
        Me.tb_fname.TabIndex = 1
        '
        'bn_prev
        '
        Me.bn_prev.Location = New System.Drawing.Point(170, 280)
        Me.bn_prev.Name = "bn_prev"
        Me.bn_prev.Size = New System.Drawing.Size(75, 32)
        Me.bn_prev.TabIndex = 2
        Me.bn_prev.Text = "Prev"
        Me.bn_prev.UseVisualStyleBackColor = True
        '
        'tb_index
        '
        Me.tb_index.Location = New System.Drawing.Point(80, 10)
        Me.tb_index.Name = "tb_index"
        Me.tb_index.Size = New System.Drawing.Size(58, 20)
        Me.tb_index.TabIndex = 3
        '
        'PrimaryKey
        '
        Me.PrimaryKey.AutoSize = True
        Me.PrimaryKey.Location = New System.Drawing.Point(12, 13)
        Me.PrimaryKey.Name = "PrimaryKey"
        Me.PrimaryKey.Size = New System.Drawing.Size(62, 13)
        Me.PrimaryKey.TabIndex = 4
        Me.PrimaryKey.Text = "Primary Key"
        '
        'FirstName
        '
        Me.FirstName.AutoSize = True
        Me.FirstName.Location = New System.Drawing.Point(21, 41)
        Me.FirstName.Name = "FirstName"
        Me.FirstName.Size = New System.Drawing.Size(57, 13)
        Me.FirstName.TabIndex = 5
        Me.FirstName.Text = "First Name"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.bn_browse)
        Me.Panel1.Controls.Add(Me.csvimport)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Location = New System.Drawing.Point(23, 318)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(345, 64)
        Me.Panel1.TabIndex = 6
        '
        'bn_browse
        '
        Me.bn_browse.Location = New System.Drawing.Point(271, 28)
        Me.bn_browse.Name = "bn_browse"
        Me.bn_browse.Size = New System.Drawing.Size(69, 23)
        Me.bn_browse.TabIndex = 2
        Me.bn_browse.Text = "Browse"
        Me.bn_browse.UseVisualStyleBackColor = True
        '
        'csvimport
        '
        Me.csvimport.Location = New System.Drawing.Point(7, 30)
        Me.csvimport.Name = "csvimport"
        Me.csvimport.Size = New System.Drawing.Size(258, 20)
        Me.csvimport.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(4, 4)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(94, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "CSV File to Import:"
        '
        'tb_lname
        '
        Me.tb_lname.Location = New System.Drawing.Point(80, 64)
        Me.tb_lname.Name = "tb_lname"
        Me.tb_lname.Size = New System.Drawing.Size(276, 20)
        Me.tb_lname.TabIndex = 7
        '
        'LastName
        '
        Me.LastName.AutoSize = True
        Me.LastName.Location = New System.Drawing.Point(20, 67)
        Me.LastName.Name = "LastName"
        Me.LastName.Size = New System.Drawing.Size(58, 13)
        Me.LastName.TabIndex = 8
        Me.LastName.Text = "Last Name"
        '
        'tb_snum
        '
        Me.tb_snum.Location = New System.Drawing.Point(80, 90)
        Me.tb_snum.Name = "tb_snum"
        Me.tb_snum.Size = New System.Drawing.Size(276, 20)
        Me.tb_snum.TabIndex = 9
        '
        'tb_city
        '
        Me.tb_city.Location = New System.Drawing.Point(80, 116)
        Me.tb_city.Name = "tb_city"
        Me.tb_city.Size = New System.Drawing.Size(276, 20)
        Me.tb_city.TabIndex = 10
        '
        'tb_province
        '
        Me.tb_province.Location = New System.Drawing.Point(80, 142)
        Me.tb_province.Name = "tb_province"
        Me.tb_province.Size = New System.Drawing.Size(276, 20)
        Me.tb_province.TabIndex = 11
        '
        'StreetNumber
        '
        Me.StreetNumber.AutoSize = True
        Me.StreetNumber.Location = New System.Drawing.Point(3, 93)
        Me.StreetNumber.Name = "StreetNumber"
        Me.StreetNumber.Size = New System.Drawing.Size(75, 13)
        Me.StreetNumber.TabIndex = 12
        Me.StreetNumber.Text = "Street Number"
        '
        'City
        '
        Me.City.AutoSize = True
        Me.City.Location = New System.Drawing.Point(50, 119)
        Me.City.Name = "City"
        Me.City.Size = New System.Drawing.Size(24, 13)
        Me.City.TabIndex = 13
        Me.City.Text = "City"
        '
        'Province
        '
        Me.Province.AutoSize = True
        Me.Province.Location = New System.Drawing.Point(29, 145)
        Me.Province.Name = "Province"
        Me.Province.Size = New System.Drawing.Size(49, 13)
        Me.Province.TabIndex = 14
        Me.Province.Text = "Province"
        '
        'tb_country
        '
        Me.tb_country.Location = New System.Drawing.Point(80, 168)
        Me.tb_country.Name = "tb_country"
        Me.tb_country.Size = New System.Drawing.Size(276, 20)
        Me.tb_country.TabIndex = 15
        '
        'Country
        '
        Me.Country.AutoSize = True
        Me.Country.Location = New System.Drawing.Point(35, 171)
        Me.Country.Name = "Country"
        Me.Country.Size = New System.Drawing.Size(43, 13)
        Me.Country.TabIndex = 16
        Me.Country.Text = "Country"
        '
        'PostalCode
        '
        Me.PostalCode.AutoSize = True
        Me.PostalCode.Location = New System.Drawing.Point(14, 197)
        Me.PostalCode.Name = "PostalCode"
        Me.PostalCode.Size = New System.Drawing.Size(64, 13)
        Me.PostalCode.TabIndex = 17
        Me.PostalCode.Text = "Postal Code"
        '
        'PhoneNumber
        '
        Me.PhoneNumber.AutoSize = True
        Me.PhoneNumber.Location = New System.Drawing.Point(3, 226)
        Me.PhoneNumber.Name = "PhoneNumber"
        Me.PhoneNumber.Size = New System.Drawing.Size(78, 13)
        Me.PhoneNumber.TabIndex = 18
        Me.PhoneNumber.Text = "Phone Number"
        '
        'EmailAddress
        '
        Me.EmailAddress.AutoSize = True
        Me.EmailAddress.Location = New System.Drawing.Point(5, 257)
        Me.EmailAddress.Name = "EmailAddress"
        Me.EmailAddress.Size = New System.Drawing.Size(73, 13)
        Me.EmailAddress.TabIndex = 19
        Me.EmailAddress.Text = "Email Address"
        '
        'tb_postalcode
        '
        Me.tb_postalcode.Location = New System.Drawing.Point(80, 194)
        Me.tb_postalcode.Name = "tb_postalcode"
        Me.tb_postalcode.Size = New System.Drawing.Size(276, 20)
        Me.tb_postalcode.TabIndex = 20
        '
        'tb_phonenumber
        '
        Me.tb_phonenumber.Location = New System.Drawing.Point(80, 223)
        Me.tb_phonenumber.Name = "tb_phonenumber"
        Me.tb_phonenumber.Size = New System.Drawing.Size(276, 20)
        Me.tb_phonenumber.TabIndex = 21
        '
        'tb_email
        '
        Me.tb_email.Location = New System.Drawing.Point(80, 250)
        Me.tb_email.Name = "tb_email"
        Me.tb_email.Size = New System.Drawing.Size(276, 20)
        Me.tb_email.TabIndex = 22
        '
        'bn_upload
        '
        Me.bn_upload.Location = New System.Drawing.Point(170, 406)
        Me.bn_upload.Name = "bn_upload"
        Me.bn_upload.Size = New System.Drawing.Size(75, 27)
        Me.bn_upload.TabIndex = 23
        Me.bn_upload.Text = "Upload"
        Me.bn_upload.UseVisualStyleBackColor = True
        '
        'tb_status
        '
        Me.tb_status.AutoSize = True
        Me.tb_status.Location = New System.Drawing.Point(50, 449)
        Me.tb_status.Name = "tb_status"
        Me.tb_status.Size = New System.Drawing.Size(37, 13)
        Me.tb_status.TabIndex = 24
        Me.tb_status.Text = "Status"
        '
        'Status
        '
        Me.Status.Location = New System.Drawing.Point(93, 446)
        Me.Status.Name = "Status"
        Me.Status.Size = New System.Drawing.Size(242, 20)
        Me.Status.TabIndex = 25
        '
        'ContactForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(421, 486)
        Me.Controls.Add(Me.Status)
        Me.Controls.Add(Me.tb_status)
        Me.Controls.Add(Me.bn_upload)
        Me.Controls.Add(Me.tb_email)
        Me.Controls.Add(Me.tb_phonenumber)
        Me.Controls.Add(Me.tb_postalcode)
        Me.Controls.Add(Me.EmailAddress)
        Me.Controls.Add(Me.PhoneNumber)
        Me.Controls.Add(Me.PostalCode)
        Me.Controls.Add(Me.Country)
        Me.Controls.Add(Me.tb_country)
        Me.Controls.Add(Me.Province)
        Me.Controls.Add(Me.City)
        Me.Controls.Add(Me.StreetNumber)
        Me.Controls.Add(Me.tb_province)
        Me.Controls.Add(Me.tb_city)
        Me.Controls.Add(Me.tb_snum)
        Me.Controls.Add(Me.LastName)
        Me.Controls.Add(Me.tb_lname)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.FirstName)
        Me.Controls.Add(Me.PrimaryKey)
        Me.Controls.Add(Me.tb_index)
        Me.Controls.Add(Me.bn_prev)
        Me.Controls.Add(Me.tb_fname)
        Me.Controls.Add(Me.bn_next)
        Me.Name = "ContactForm"
        Me.Text = "CustomerContactForm"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents bn_next As Button
    Friend WithEvents tb_fname As TextBox
    Friend WithEvents bn_prev As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents tb_index As TextBox
    Friend WithEvents PrimaryKey As Label
    Friend WithEvents FirstName As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents bn_browse As Button
    Friend WithEvents csvimport As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents tb_lname As TextBox
    Friend WithEvents LastName As Label
    Friend WithEvents tb_snum As TextBox
    Friend WithEvents tb_city As TextBox
    Friend WithEvents tb_province As TextBox
    Friend WithEvents StreetNumber As Label
    Friend WithEvents City As Label
    Friend WithEvents Province As Label
    Friend WithEvents tb_country As TextBox
    Friend WithEvents Country As Label
    Friend WithEvents PostalCode As Label
    Friend WithEvents PhoneNumber As Label
    Friend WithEvents EmailAddress As Label
    Friend WithEvents tb_postalcode As TextBox
    Friend WithEvents tb_phonenumber As TextBox
    Friend WithEvents tb_email As TextBox
    Friend WithEvents bn_upload As Button
    Friend WithEvents tb_status As Label
    Friend WithEvents Status As TextBox
End Class
